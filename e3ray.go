package ga

/* tools */

/* E3HPoint, helper function to apply the details of HPoint to an E3 Point */
func E3HPoint(hpoint [4]float64) [3]float64 {
	return [3]float64{(hpoint[0] * hpoint[3]), (hpoint[1] * hpoint[3]), (hpoint[2] * hpoint[3])}
}

type E3Segment [10]float64 /* R3 Multivector + tmin + tmax */

/* E3Point */
func (segment E3Segment) E3Point(t float64) [3]float64 {

	return [3]float64{segment[1], segment[2], (segment[3] * t)}
}

/* E3HPoint, or weighted point. To view actual E3Point=(x * w) + (y * w) + (z * w) */
func (segment E3Segment) E3HPoint(t float64) [4]float64 {

	h := 1.0
	if segment[8] > t || t > segment[9] {
		h = 0.0
	}

	return [4]float64{segment[1], segment[2], (segment[3] * t), h}
}

/* Limits, get the min and max of the segment */
func (segment E3Segment) Limits() [2]float64 {
	return [2]float64{segment[8], segment[9]}
}

func (segment E3Segment) Within(t float64) bool {

	return (segment[8] <= t && t <= segment[9])
}

/* ConstructE3Segment */
func ConstructE3Segment(space R3Space, A, B R2Multivector, min, max float64) E3Segment {

	r := ConstructE3Ray(space, A, B)

	s := [10]float64{0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
	for i := 0; i < 8; i++ {

		s[i] = r[i]
	}

	s[8] = min
	s[9] = max

	return s
}

type E3Ray [8]float64 /* basically a R3 Multivector */

func (space R3Space) E3Ray(x, y, z float64) E3Ray {

	m := space.E3Point(x, y, z)
	return E3Ray{m[0], m[1], m[2], m[3], m[4], m[5], m[6], m[7]}
}

func (ray E3Ray) E3Point(t float64) [3]float64 {

	return [3]float64{ray[1] * ray[0], ray[2] * ray[0], ((ray[3] * ray[0]) * t)}
}

func (ray E3Ray) E3HPoint(t float64) [4]float64 {

	return [4]float64{ray[1], ray[2], (ray[3] * t), 1.0}
}

/* ConstructAsE3Segment */
func (ray E3Ray) ConstrainAsE3Segment(min, max float64) E3Segment {

	s := [10]float64{0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
	for i := 0; i < 8; i++ {

		s[i] = ray[i]
	}

	s[8] = min
	s[9] = max

	return s
}

/* ConstructE3Ray */
func ConstructE3Ray(space R3Space, A, B R2Multivector) E3Ray {

	r := space.ReduceToR2Space()

	AB := r.Geometric(A, B)

	r1 := 1.0
	r2 := AB[2]
	r3 := AB[3]
	r4 := 1.0
	r5 := r.e12
	r6 := r.e12
	r7 := r.e12
	r8 := 0.0

	return [8]float64{r1, r2, r3, r4, r5, r6, r7, r8}
}
