package ga

import (
	"fmt"
	. "github.com/smartystreets/goconvey/convey"
	"math/rand"
	"testing"
)

func Test_R3Tools(t *testing.T) {

	Convey("R3 Tools", t, func() {

		d := distance([3]float64{0, 0, 0}, [3]float64{5, 5, 5})
		So(d, ShouldEqual, 8.660254037844387)

		space := R3UnitSpace
		p := space.E3Point(0, 0, 0)
		a := space.E3Point(5, 5, 5)

		product := space.Geometric(p, a)
		fmt.Printf("product = %v\n", product)
		So(space.Scalar(product), ShouldEqual, d)

	})

	Convey("R3 Tools -- random distances", t, func() {

		space := R3UnitSpace
		u := space.E3Point(0, 0, 0)

		for i := 0; i < 1000; i++ {
			ur := rand.ExpFloat64()
			p := [3]float64{ur, ur, ur}
			d := distance([3]float64{0, 0, 0}, p)

			a := space.E3Point(p[0], p[1], p[2])
			product := space.Geometric(u, a)
			So(space.Scalar(product), ShouldEqual, d)
			//fmt.Printf("p=%v, d %f == product %f\n",p,d,space.Scalar(product))
		}
	})

	Convey("R3 Sphere", t, func() {

		space := R3UnitSpace
		sphere := space.E3Sphere(0, 0, 0, 5.0)

		fmt.Printf("sphere = %v\n", sphere)

		p0 := space.E3Point(1, 1, 1)
		p1 := space.E3Point(5, 5, 5)
		p2 := space.E3Point(10, 10, 10)

		fmt.Printf("p0=%v\np1=%v\np2=%v\n", p0, p1, p2)

		i, d := space.SpherePointIntersection(sphere, p0)
		fmt.Printf("i=%s,d=%f\n", i, d)
		So(i, ShouldEqual, Within)
		So(d, ShouldEqual, distance(sphere.XYZ(), p0.XYZ()))

		i, d = space.SpherePointIntersection(sphere, p1)
		fmt.Printf("i=%s,d=%f\n", i, d)
		So(i, ShouldEqual, Without)
		So(d, ShouldEqual, distance(sphere.XYZ(), p1.XYZ()))

		i, d = space.SpherePointIntersection(sphere, p2)
		fmt.Printf("i=%s,d=%f\n", i, d)
		So(i, ShouldEqual, Without)
		So(d, ShouldEqual, distance(sphere.XYZ(), p2.XYZ()))
	})
}
