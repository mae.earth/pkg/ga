# Geometric Algebra
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/mae.earth/pkg/ga)](https://goreportcard.com/report/gitlab.com/mae.earth/pkg/ga)

Geometric Algebra implementation in golang, currently the code only supports R2 and R3 spaces. Note, this is very much a research project, do not
use for production. The aim will be to construct a domain specific language to better support the math directly rather then let golang get in the way. 
