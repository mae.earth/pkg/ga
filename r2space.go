package ga

import (
	"math"
)

/* we construct concrete scalar, vector, trivector via a multivector through space */

type R2Multivector [4]float64

type R2Space struct {
	e1 float64
	e2 float64

	e12  float64 /* identity bivector */
	ie12 float64 /* inverse identity bivector */
}

var (
	R2UnitSpace = R2Space{e1: 1, e2: 1, e12: 1, ie12: -1}
)

/* Euclidean 2-d Point */
func (space R2Space) E2Point(x, y float64) R2Multivector {

	/* FIXME: check for zero */
	s := math.Sqrt(x*x + y*y)
	nx := x / s
	ny := y / s

	return [4]float64{s, nx, ny, 1.0}
}

/* Euclidean 2-d Vector */
func (space R2Space) E2Vector(x, y float64) R2Multivector {

	s := math.Sqrt(x*x + y*y)
	nx := x / s
	ny := y / s

	return [4]float64{s, nx, ny, 0.0}
}

/* Euclidean 2-d Ray */
/*func (space R2Space) E2Ray(x,y float64) R3Multivector {
	TODO: notice that we return an R3 Multivector, basically because a ray is a snapshot in time (z)
}*/

func (space R2Space) Scalar(mv R2Multivector) float64 {

	return mv[0]
}

func (space R2Space) Vector(mv R2Multivector) [2]float64 {

	return [2]float64{(mv[1] * space.e1), (mv[2] * space.e2)}
}

func (space R2Space) Bivector(mv R2Multivector) float64 {

	return mv[3] * space.e12
}

func (space R2Space) AreEqual(A, B R2Multivector) bool {

	return (A[0] == B[0] && A[1] == B[1] && A[2] == B[2] && A[3] == B[3])
}

/* basically decompossing to R1 */
func (space R2Space) Outer(A, B R2Multivector) R2Multivector {

	return [4]float64{(A[1]*B[2] - A[2]*B[1]) * space.e12, 0, 0, 0}
}

func (space R2Space) Inner(A, B R2Multivector) R2Multivector {

	o := [4]float64{0, 0, 0, 0}
	for i := 0; i < 4; i++ {
		o[i] = A[i] * B[i]
	}
	return o
}

func (space R2Space) Geometric(A, B R2Multivector) R2Multivector {

	i := space.Inner(A, B)
	o := space.Outer(A, B)

	return [4]float64{(i[0] + o[0]), (i[1] + o[1]), (i[2] + o[2]), (i[3] + o[3])}
}

/* R2Space */
func NewR2Space(e1, e2 float64) R2Space {

	e12 := e1 * e2 * e1 * e2
	ie12 := (-e2) * e2

	return R2Space{e1: e1, e2: e2, e12: e12, ie12: ie12}
}
