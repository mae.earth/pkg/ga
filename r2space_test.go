package ga

import (
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func Test_E2Basic(t *testing.T) {

	Convey("E2 Point", t, func() {

		space := R2UnitSpace
		So(space, ShouldNotBeNil)

		p := space.E2Point(4, 5)
		So(p, ShouldEqual, R2Multivector([4]float64{6.4031242374328485, 0.6246950475544243, 0.7808688094430304, 1}))
		So(p[1]*p[0], ShouldEqual, 4) /* x */
		So(p[2]*p[0], ShouldEqual, 5) /* y */

	})

	Convey("E2 Vector", t, func() {

		space := R2UnitSpace
		So(space, ShouldNotBeNil)

		v := space.E2Vector(4, 5)
		So(v, ShouldEqual, R2Multivector([4]float64{6.4031242374328485, 0.6246950475544243, 0.7808688094430304, 0})) /* notice the trailing zero */
		So(v[1]*v[0], ShouldEqual, 4)                                                                                /* x */
		So(v[2]*v[0], ShouldEqual, 5)                                                                                /* y */
	})

}

func Test_R2Basic(t *testing.T) {

	Convey("R2 Basic", t, func() {

		space := NewR2Space(1, 1)
		So(space, ShouldNotBeNil)

		mv := R2Multivector([4]float64{0, 1, 2, 3})
		So(mv, ShouldNotBeEmpty)

		So(space.e12, ShouldEqual, 1.0)
		So(space.ie12, ShouldEqual, -1.0)
	})

	Convey("R2 Outer Product A^B", t, func() {

		space := NewR2Space(1, 1)
		So(space, ShouldNotBeNil)

		A := R2Multivector([4]float64{0, 1, 3, 0})
		B := R2Multivector([4]float64{0, 3, 1, 0})

		So(space.AreEqual(A, B), ShouldBeFalse)

		So(space.Outer(A, B), ShouldEqual, R2Multivector([4]float64{-8, 0, 0, 0})) /* FIXME: formal check */
	})

	Convey("R2 Inner Product A¬B", t, func() {

		space := NewR2Space(1, 1)
		So(space, ShouldNotBeNil)

		A := R2Multivector([4]float64{0, 1, 3, 0})
		B := R2Multivector([4]float64{0, 3, 1, 0})

		So(space.AreEqual(A, B), ShouldBeFalse)

		So(space.Inner(A, B), ShouldEqual, R2Multivector([4]float64{0, 3, 3, 0})) /* FIXME: formal check */
	})

	Convey("R2 Geometric Product AB", t, func() {

		space := NewR2Space(1, 1)
		So(space, ShouldNotBeNil)

		A := R2Multivector([4]float64{0, 1, 3, 0})
		B := R2Multivector([4]float64{0, 3, 1, 0})

		So(space.AreEqual(A, B), ShouldBeFalse)

		So(space.Geometric(A, B), ShouldEqual, R2Multivector([4]float64{-8, 3, 3, 0})) /* FIXME: formal check */
	})

	Convey("R2 concrete sets", t, func() {

		space := NewR2Space(1, 1)
		So(space, ShouldNotBeNil)

		A := R2Multivector([4]float64{5.67, 1, 3, 0})
		B := R2Multivector([4]float64{1.23, 3, 1, 1.67})

		So(space.Scalar(A), ShouldEqual, 5.67)
		So(space.Vector(A), ShouldEqual, [2]float64{1, 3})
		So(space.Bivector(A), ShouldEqual, 0.0)

		So(space.Scalar(B), ShouldEqual, 1.23)
		So(space.Vector(B), ShouldEqual, [2]float64{3, 1})
		So(space.Bivector(B), ShouldEqual, 1.67)

	})

}
