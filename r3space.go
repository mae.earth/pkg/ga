package ga

import (
	"math"
)

/* R3MultivectorSpec */
type R3MultivectorSpec struct {
	e1 float64 /* scalar */
	e2 float64 /* a1 or x */
	e3 float64 /* a2 or y */
	e4 float64 /* a3 or z */
	e5 float64 /* e12 bivector */
	e6 float64 /* e23 bivector */
	e7 float64 /* e31 bivector */
	e8 float64 /* e123 trivector */
}

/* R3Multivector */
type R3Multivector [8]float64

/* XYZ */
func (m R3Multivector) XYZ() [3]float64 { return [3]float64{m[1] * m[0], m[2] * m[0], m[3] * m[0]} }

/* R3Space */
type R3Space struct {
	e1 float64
	e2 float64
	e3 float64

	e12  float64
	ie12 float64 /* inverse */

	e23  float64
	ie23 float64

	e31  float64
	ie31 float64

	e123 float64
}

var (
	R3UnitSpace = R3Space{e1: 1, e2: 1, e3: 1, e12: 1, ie12: -1, e23: 1, ie23: -1, e31: 1, ie31: -1, e123: 1}
)

/* E3ZeroPoint */
func (space R3Space) E3ZeroPoint() R3Multivector {
	return space.E3Point(0.0, 0.0, 0.0)
}

func (space R3Space) E3Scalar(s float64) R3Multivector {
	return [8]float64{s, 0, 0, 0, space.e12, space.e23, space.e31, 1} /* TODO: is the last r 1.0 or 0.0 */
}
	

/* E3Point, useful helper function for creating a point */
func (space R3Space) E3Point(x, y, z float64) R3Multivector {

	nx := 0.0
	ny := 0.0
	nz := 0.0
	s := 1.0

//	if x+y+z != 0.000000 {

		s = math.Sqrt((x * x) + (y * y) + (z * z))

		nx = x / s
		ny = y / s
		nz = z / s
	//} 		

	if x == y && y == z && z == 0.0000000 {
		s = 0.0
	}
		

	return [8]float64{s, nx, ny, nz, space.e12, space.e23, space.e31, 1}
}

/* E3Pointv */
func (space R3Space) E3Pointv(v [3]float64) R3Multivector {
	return space.E3Point(v[0], v[1], v[2])
}

/* E3HPoint */
func (space R3Space) E3HPoint(x, y, z, w float64) R3Multivector {

	s := math.Sqrt((x * x) + (y * y) + (z * z))
	nx := x / s
	ny := y / s
	nz := y / s

	return [8]float64{s, nx, ny, nz, space.e12, space.e23, space.e31, w}
}

/* E3Vector */
func (space R3Space) E3Vector(x, y, z float64) R3Multivector {

	s := math.Sqrt((x * x) + (y * y) + (z * z))
	nx := x / s
	ny := y / s
	nz := z / s

	return [8]float64{s, nx, ny, nz, space.e12, space.e23, space.e31, 0}
}

func (space R3Space) E3Vectorv(v [3]float64) R3Multivector {
	return space.E3Vector(v[0],v[1],v[2])
}

/* Scalar */
func (space R3Space) Scalar(mv R3Multivector) float64 {
	return mv[0]
}

/* Vector, helper function (not the same as E3Vector) */
func (space R3Space) Vector(mv R3Multivector) [3]float64 {
	return [3]float64{(mv[1] * space.e1), (mv[2] * space.e2), (mv[3] * space.e3)}
}

/* Bivector, helper function */
func (space R3Space) Bivector(mv R3Multivector) [3]float64 {
	return [3]float64{(mv[4] * space.e12), (mv[5] * space.e23), (mv[6] * space.e31)}
}

/* Trivector, helper function */
func (space R3Space) Trivector(mv R3Multivector) float64 {
	return mv[7] * space.e123
}

/* AreEqual, does a single component-wise equality check between two R3 multivectors */
func (space R3Space) AreEqual(A, B R3Multivector) bool {

	for i := 0; i < 8; i++ {
		if A[i] != B[i] {
			return false
		}
	}
	return true
}

/* Outer, a | b basically decomposing to R2 */
func (space R3Space) Outer(A, B R3Multivector) R3Multivector {
	return [8]float64{(A[1]*B[2] - A[2]*B[1]) * space.e12,
		(A[2]*B[3] - A[3]*B[2]) * space.e23,
		(A[3]*B[1] - A[1]*B[3]) * space.e31,
		0, 0, 0, 0, 0}
}

/* Inner, AB = A · B */
func (space R3Space) Inner(A, B R3Multivector) R3Multivector {
	o := [8]float64{0, 0, 0, 0, 0, 0, 0, 0}
	for i := 0; i < 8; i++ {
		o[i] = A[i] * B[i]
	}
	return o
}

/* Geometric a · b + a | b*/
func (space R3Space) Geometric(A, B R3Multivector) R3Multivector {

	i := space.Inner(A, B)
	o := space.Outer(A, B)

	out := [8]float64{0, 0, 0, 0, 0, 0, 0, 0}
	for j := 0; j < 8; j++ {
		out[j] = i[j] + o[j]
	}

	return out
}

/* ReduceToR2Space */
func (space R3Space) ReduceToR2Space() R2Space {
	return NewR2Space(space.e1, space.e2) /* we just drop e3 -- not the best answer, but works for unit spaces */
}

/* NewR3Space */
func NewR3Space(e1, e2, e3 float64) R3Space {

	e12 := e1 * e2 * e1 * e2
	ie12 := (-e2) * e2
	e23 := e2 * e3 * e2 * e3
	ie23 := (-e3) * e2
	e31 := e3 * e1 * e3 * e1
	ie31 := (-e1) * e1

	return R3Space{e1, e2, e3, e12, ie12, e23, ie23, e31, ie31, 1.0} /* TODO: what is e123 */
}


/* DSL can allow the user to setup as they like using symbols as they please */

/* example DSL : 
 *
 * · is inner product
 * ^ is outer product
 * & is geometric product
 *
 * a : R³ [1 1 1]
 * b : R³ [2 2 2] 
 *
 * ab = a · b
 * ab = a ^ b
 * ab = a · b + a ^ b || a & b
 *
 */
 


