package ga

import (
	"fmt"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func Test_E3Basic(t *testing.T) {

	Convey("E3 Point", t, func() {

		space := R3UnitSpace
		So(space, ShouldNotBeNil)

		p := space.E3Point(4, 5, 0)
		So(p, ShouldEqual, R3Multivector([8]float64{6.4031242374328485, 0.6246950475544243, 0.7808688094430304, 0, 1, 1, 1, 1}))
		So(p[1]*p[0], ShouldEqual, 4) /* x */
		So(p[2]*p[0], ShouldEqual, 5) /* y */
		So(p[3]*p[0], ShouldEqual, 0) /* z */

	})

	Convey("E3 Vector", t, func() {

		space := R3UnitSpace
		So(space, ShouldNotBeNil)

		v := space.E3Vector(4, 5, 0)
		So(v, ShouldEqual, R3Multivector([8]float64{6.4031242374328485, 0.6246950475544243, 0.7808688094430304, 0, 1, 1, 1, 0})) /* notice the trailing zero */
		So(v[1]*v[0], ShouldEqual, 4)                                                                                            /* x */
		So(v[2]*v[0], ShouldEqual, 5)                                                                                            /* y */
		So(v[3]*v[0], ShouldEqual, 0)                                                                                            /* z */
	})
}

func Test_R3Basic(t *testing.T) {

	Convey("R3 Basic", t, func() {

		space := NewR3Space(1, 1, 1)
		So(space, ShouldNotBeNil)

		mv := R3Multivector([8]float64{0, 1, 2, 3, 1, 1, 1, 1})
		So(mv, ShouldNotBeEmpty)

		So(space.e12, ShouldEqual, 1.0)
		So(space.ie12, ShouldEqual, -1.0)

		So(space.e23, ShouldEqual, 1.0)
		So(space.ie23, ShouldEqual, -1.0)

		So(space.e31, ShouldEqual, 1.0)
		So(space.ie31, ShouldEqual, -1.0)
	})

	Convey("R3 Outer Product A^B", t, func() {

		space := NewR3Space(1, 1, 1)
		So(space, ShouldNotBeNil)

		A := R3Multivector([8]float64{0, 1, 3, 0, 1, 1, 1, 1})
		B := R3Multivector([8]float64{0, 3, 1, 0, 1, 1, 1, 1})

		So(space.AreEqual(A, B), ShouldBeFalse)

		So(space.Outer(A, B), ShouldEqual, R3Multivector([8]float64{-8, 0, 0, 0, 0, 0, 0, 0})) /* FIXME: formal check */
	})

	Convey("R3 Inner Product A¬B", t, func() {

		space := NewR3Space(1, 1, 1)
		So(space, ShouldNotBeNil)

		A := R3Multivector([8]float64{0, 1, 3, 0, 1, 1, 1, 1})
		B := R3Multivector([8]float64{0, 3, 1, 0, 1, 1, 1, 1})

		So(space.AreEqual(A, B), ShouldBeFalse)

		So(space.Inner(A, B), ShouldEqual, R3Multivector([8]float64{0, 3, 3, 0, 1, 1, 1, 1})) /* FIXME: formal check */
	})

	Convey("R3 Geometric Product AB", t, func() {

		space := NewR3Space(1, 1, 1)
		So(space, ShouldNotBeNil)

		A := R3Multivector([8]float64{0, 1, 3, 0, 1, 1, 1, 1})
		B := R3Multivector([8]float64{0, 3, 1, 0, 1, 1, 1, 1})

		So(space.AreEqual(A, B), ShouldBeFalse)

		So(space.Geometric(A, B), ShouldEqual, R3Multivector([8]float64{-8, 3, 3, 0, 1, 1, 1, 1})) /* FIXME: formal check */
	})

	Convey("R3 concrete sets", t, func() {

		space := NewR3Space(1, 1, 1)
		So(space, ShouldNotBeNil)

		A := R3Multivector([8]float64{5.67, 1, 3, 0, 1, 1, 1, 1})
		B := R3Multivector([8]float64{1.23, 3, 1, 1.67, 1, 1, 1, 1})

		So(space.Scalar(A), ShouldEqual, 5.67)
		So(space.Vector(A), ShouldEqual, [3]float64{1, 3, 0})
		So(space.Bivector(A), ShouldEqual, [3]float64{1, 1, 1})
		So(space.Trivector(A), ShouldEqual, 1.0)

		So(space.Scalar(B), ShouldEqual, 1.23)
		So(space.Vector(B), ShouldEqual, [3]float64{3, 1, 1.67})
		So(space.Bivector(B), ShouldEqual, [3]float64{1, 1, 1})
		So(space.Trivector(B), ShouldEqual, 1.0)

	})

	Convey("R3 Distances", t, func() {

		space := NewR3Space(1, 1, 1)
		So(space, ShouldNotBeNil)

		A := space.E3Point(1, 1, 1)
		B := space.E3Point(0, 0, 0)

		ab := space.Inner(A, B)

		d := distance([3]float64{1, 1, 1}, [3]float64{0, 0, 0})

		So(ab[0], ShouldEqual, d)

		Convey("Vector Space Tool", func() {

			vspace := ToVectorSpace(space)
			So(vspace.E3DistanceBetween([3]float64{1, 1, 1}, [3]float64{0, 0, 0}), ShouldEqual, d)
			So(vspace.DistanceBetween(A, B), ShouldEqual, d)
		})

	})

	Convey("R3 Barycentrics", t, func() {

		space := NewR3Space(1, 1, 1)
		So(space, ShouldNotBeNil)

		A := space.E3Point(1, 1, 1)
		So(A.XYZ(),ShouldEqual,[3]float64{1,1,1})
		B := space.E3Point(0, 0, 0)
		So(B.XYZ(),ShouldEqual,[3]float64{0,0,0})

		vspace := ToVectorSpace(space)

		C := vspace.Barycentric(vspace.M(A, B), 1.0)
		So(C, ShouldNotEqual, R3Multivector{0, 0, 0, 0, 0, 0, 0, 0})

		So(C.XYZ(), ShouldEqual, [3]float64{0.538675134594813, 0.538675134594813, 0.538675134594813}) /* TODO: not sure */

		C = vspace.E3Barycentric(vspace.E3M([3]float64{1, 1, 1}, [3]float64{0, 0, 0}), 1.0)
		So(C.XYZ(), ShouldEqual, [3]float64{0.538675134594813, 0.538675134594813, 0.538675134594813}) /* TODO: not sure */

		Convey("3 points", func() {

			A := vspace.E3Point(1, 1, 1)
			B := vspace.E3Point(0, 0, 0)
			C := vspace.E3Point(-1, -1, -1)

			/* push towards -1 */
			D := vspace.Barycentric(vspace.M(A, B, C), 1.0, 1.0, 2.0)
			So(D.XYZ(), ShouldEqual, [3]float64{-0.46163339315325014, -0.46163339315325014, -0.46163339315325014})

			E := vspace.AutoMaxZeroBarycentric(A, B, C, D)
			So(E.XYZ(), ShouldEqual, [3]float64{-0.07598102265667697, -0.07598102265667697, -0.07598102265667697})

			E = vspace.AutoMaxArbBarycentric(A, A, B, C, D)
			So(E.XYZ(), ShouldEqual, [3]float64{-0.07598102265667697, -0.07598102265667697, -0.07598102265667697})
		})
	})

	Convey("R3 vectors", t, func() {

		space := NewR3Space(1, 1, 1)
		So(space, ShouldNotBeNil)

		A := space.E3Point(0, 0, 0)
		B := space.E3Point(5, 5, 5)

		v := space.Inner(A, B)

		fmt.Printf("A = %v\nB = %v\n", A, B)
		fmt.Printf("v = %v\n", v)

		vspace := ToVectorSpace(space)

		fmt.Printf("|A-B| = %v\n", vspace.VectorBetween(A, B))
		fmt.Printf("|B-A| = %v\n", vspace.VectorBetween(B, A))

	})

}
