package ga

import (
	"fmt"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

var (
	r = []float64{-1.0, -0.9, -0.8, -0.7, -0.6, -0.5, -0.4, -0.3, -0.2, -0.1, 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0}
)

func Test_E3Ray(t *testing.T) {

	Convey("E3 Ray", t, func() {

		/* construct an E3 ray from a R2 space (bivector) + R2 vector, we cheat and build a R3 space */
		space := R3UnitSpace

		A := R2Multivector([4]float64{1.0, 2.5, 3, 1.0}) /* xy = (2.5,3) */
		B := R2Multivector([4]float64{1.0, 0, 1.5, 1.0}) /* xy = (0,1.5) */

		ray := ConstructE3Ray(space, A, B)
		So(ray, ShouldEqual, E3Ray([8]float64{1.0, 4.5, 1, 1, 1, 1, 1, 0})) /* TODO: formal proof required */

		for i := 0; i < 10; i++ {

			t := float64(i) + .5

			So(ray.E3Point(t), ShouldEqual, [3]float64{4.5, 1, t})
		}

		for i := 0; i > -10; i-- {

			t := float64(i) + .5

			So(ray.E3Point(t), ShouldEqual, [3]float64{4.5, 1, t})
		}

		segment := ray.ConstrainAsE3Segment(8.0, 9.0)
		So(segment, ShouldEqual, E3Segment([10]float64{1, 4.5, 1, 1, 1, 1, 1, 0, 8, 9}))
	})

	Convey("E3 Segment", t, func() {

		space := R3UnitSpace

		A := R2Multivector([4]float64{1.0, 2.5, 3, 1.0}) /* xy = (2.5,3) */
		B := R2Multivector([4]float64{1.0, 0, 1.5, 1.0}) /* xy = (0,1.5) */

		segment := ConstructE3Segment(space, A, B, 3.5, 10.2)
		So(segment, ShouldEqual, E3Segment([10]float64{1, 4.5, 1, 1, 1, 1, 1, 0, 3.5, 10.2}))
		So(segment.Limits(), ShouldEqual, [2]float64{3.5, 10.2})

		So(segment.Within(1.0), ShouldBeFalse)
		So(segment.E3HPoint(1.0), ShouldEqual, [4]float64{4.5, 1, 1, 0})

		So(segment.Within(11.0), ShouldBeFalse)
		So(segment.E3HPoint(11.0), ShouldEqual, [4]float64{4.5, 1, 11, 0})

		values := []float64{0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5, 11.5}
		valids := []bool{false, false, false, true, true, true, true, true, true, true, false, false}

		for i := 0; i < len(values); i++ {

			t := values[i]

			So(segment.Within(t), ShouldEqual, valids[i])

			w := 1.0
			if !valids[i] {
				w = 0.0
			}

			point := segment.E3Point(t)

			point[0] *= w
			point[1] *= w
			point[2] *= w

			So(E3HPoint(segment.E3HPoint(t)), ShouldEqual, point)
		}

	})

	Convey("E3 Sphere Intersection Test", t, func() {

		/* TODO: not even sure what this is supposed to test !? */

		space := R3UnitSpace
		sphere := space.E3Sphere(0, 0, 10, 1)

		for _, x := range r {
			for _, y := range r {

				ray := space.E3Ray(x, y, 0.0)

				for _, t := range r {

					e3 := ray.E3Point((100.0 * t))

					intersection, distance := space.SpherePointIntersection(sphere, space.E3Pointv(e3))
					if intersection == Within || intersection == Surface {

						origin := space.E3Point(x, y, 0.0)
						intersect := space.E3Pointv(e3)

						depth := space.Scalar(space.Geometric(origin, intersect))

						fmt.Printf("hit at (%.2f,%.2f) %.4f distance [%s] depth is %.4f, t=%.2f\n", x, y, distance, intersection, depth, t)
					}
				}
			}
		}

	})

}
