package ga

import (
	"math"
)

/* TODO: move sphere and ray into new space tool rather then directly in the core */

/* Just for testing */
func distance(a, b [3]float64) float64 {

	x := (a[0] - b[0]) * (a[0] - b[0])
	y := (a[1] - b[1]) * (a[1] - b[1])
	z := (a[2] - b[2]) * (a[2] - b[2])

	return math.Sqrt(x + y + z)
}

func round(a [3]float64) [3]float64 {
	return [3]float64{math.Round(a[0]), math.Round(a[1]), math.Round(a[2])}
}

/* E3Sphere */
type E3Sphere [9]float64

func (sphere E3Sphere) XYZ() [3]float64 {
	return [3]float64{sphere[1] * sphere[0], sphere[2] * sphere[0], sphere[3] * sphere[0]}
}

/* E3Sphere */
func (space R3Space) E3Sphere(x, y, z, radius float64) E3Sphere {

	m := space.E3Point(x, y, z)
	n := [9]float64{0, 0, 0, 0, 0, 0, 0, 0, 0}
	for i := 0; i < 8; i++ {
		n[i] = m[i]
	}
	n[8] = radius

	return n
}

/* Intersection */
type Intersection string

const (
	Without = Intersection("without")
	Within  = Intersection("within")
	Surface = Intersection("surface")
)

/* SpherePointIntersection */
func (space R3Space) SpherePointIntersection(sphere E3Sphere, point R3Multivector) (Intersection, float64) {

	product := space.Geometric([8]float64{sphere[0], sphere[1], sphere[2], sphere[3], sphere[4], sphere[5], sphere[6], sphere[7]}, point)
	distance := product[0]

	/* what does this even mean?
	if distance < 0.0000000 {
		distance *= -1
	} */

	if distance < sphere[8] {
		return Within, distance
	}

	if distance == sphere[8] {
		return Surface, distance
	}

	return Without, distance
}

/* toolsets TODO : move to toolset packages */
type VectorSpaceTool struct {
	R3Space
}

/* VectorBetween */
func (v VectorSpaceTool) VectorBetween(A, B R3Multivector) [3]float64 {
	/* TODO: this is the cheat, we should really do this properly in GA not VA! */
	a := A.XYZ()
	b := B.XYZ()

	x := a[0] - b[0]
	y := a[1] - b[1]
	z := a[2] - b[2]

	length := math.Sqrt((x * x) + (y * y) + (z * z))
	s := 1.0
	if length > 0.000000 {
		s /= length
	}

	x *= s
	y *= s
	z *= s

	return [3]float64{x, y, z}
}

/* M */
func (v VectorSpaceTool) M(vs ...R3Multivector) []R3Multivector {
	return vs
}

/* E3M */
func (v VectorSpaceTool) E3M(vs ...[3]float64) [][3]float64 {
	return vs
}

func (v VectorSpaceTool) Add(A,B R3Multivector) R3Multivector {
	C := R3Multivector{0,0,0,0,0,0,0,0}
	
	for i := 0; i < 8; i++ {
		C[i] = A[i] + B[i]
	}

	return C
}

func (v VectorSpaceTool) Mul(A,S R3Multivector) R3Multivector {
	C := R3Multivector{0,0,0,0,0,0,0,0}

	for i := 0; i < 8; i++ {
		C[i] = A[i] * S[i]
	}

	return C
}

/* DistanceBetween */
func (v VectorSpaceTool) DistanceBetween(A, B R3Multivector) float64 {
	return v.Inner(A, B)[0]
}

/* E3DistanceBetween */
func (v VectorSpaceTool) E3DistanceBetween(a, b [3]float64) float64 {

	A := v.E3Pointv(a)
	B := v.E3Pointv(b)

	ab := v.Inner(A, B)

	return ab[0]
}

/* Barycentric */
func (v VectorSpaceTool) Barycentric(vs []R3Multivector, weights ...float64) R3Multivector {
	/* TODO: this PROBABLY is NOT correct */
	out := v.E3ZeroPoint()
	w := 1.0

	if len(vs) == 0 {
		return out
	}

	for i, v := range vs {
		w = 1.0
		if len(weights) > i {
			w = weights[i]
		} else {
			w = weights[len(weights)-1]
		}

		for j := 0; j < 8; j++ {

			out[j] += (v[j] * w)
		}
	}

	s := 1.0 / float64(len(vs))

	for j := 0; j < 8; j++ {
		out[j] *= s
	}

	return out
}

/* E3Barycentric */
func (v VectorSpaceTool) E3Barycentric(vs [][3]float64, weights ...float64) R3Multivector {

	out := make([]R3Multivector, len(vs))

	for i := 0; i < len(vs); i++ {
		out[i] = v.E3Pointv(vs[i])
	}

	return v.Barycentric(out, weights...)
}

/* AutoBarycentric, autogenerate weights based on distance */
func (vst VectorSpaceTool) AutoMaxZeroBarycentric(vs ...R3Multivector) R3Multivector {
	return vst.AutoMaxArbBarycentric(vst.E3ZeroPoint(), vs...)
}

/* AutoMaxArbBarycentric */
func (vst VectorSpaceTool) AutoMaxArbBarycentric(arb R3Multivector, vs ...R3Multivector) R3Multivector {

	max := 0.0

	reg := func(d float64) {
		if d > max {
			max = d
		}
	}

	f := func(d float64) float64 {
		return d / max
	}

	return vst.AutoBarycentric(arb, reg, f, vs...)
}

/* AutoBarycentric */
func (vst VectorSpaceTool) AutoBarycentric(arb R3Multivector, reg func(float64), f func(float64) float64, vs ...R3Multivector) R3Multivector {

	if reg == nil {
		reg = func(float64) {}
	}

	if f == nil {
		f = func(float64) float64 {
			return 1.0
		}
	}

	rs := make([]float64, len(vs))

	for i, v := range vs {

		rs[i] = vst.DistanceBetween(arb, v)
		/* do the register */
		reg(rs[i])
	}

	for i, r := range rs {
		rs[i] = f(r)
	}

	return vst.Barycentric(vs, rs...)
}

/* ToVectorSpace */
func ToVectorSpace(space R3Space) VectorSpaceTool {
	return VectorSpaceTool{space}
}


