package ga

import (
	"fmt"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

type agent struct {
	position R3Multivector
	orientation R3Multivector
	speed R3Multivector
}


func Test_Examples(t *testing.T) {
	Convey("Examples",t,func() {
		Convey("Prey fleeing N predators",func() {
			/* For N predators (as described by a R3 point, R3 |vector| and a R3 scalar)
			 * work out the best escape vector and speed for a single prey. 
       *
       * Predator : position + orientation + speed
 			 * Prey : position + orientation + speed
       *
       * we use a barycentric model to get a centre point in regards to all
			 * agents, using the prey as the key point and distance for the weighting. 
			 *
       */

			/* NOTES : we will use R3 space (as R2 lacks the vector tool) and simply 
			 * set Y as 0.0 
			 */
			space := NewR3Space(1,1,1)
			vspace := ToVectorSpace(space)

			point := space.E3Point(-5,0,5)
			fmt.Printf("point %v XYZ(%v)\n",point,point.XYZ())

			So(point.XYZ(),ShouldEqual,[3]float64{-5,0,5})

			prey := agent{position:space.E3Point(0,0,-5), orientation:space.E3Vector(0,0,1), speed:space.E3Scalar(1.0)}

			/* predator orientated towards the prey */
			predA := agent{	position:space.E3Point(-5,0,5), 
											orientation:space.E3Vectorv(vspace.VectorBetween(space.E3Point(-5,0,5),prey.position)), 
											speed:space.E3Scalar(3.0)}

			So(predA,ShouldNotBeEmpty)

			/* do a check, project forwards position + orientation * speed */
			
			fmt.Printf("PredA position @ t0 = %v E3(%v)\n", predA.position,predA.position.XYZ())

			predA.position = vspace.Add(predA.position, vspace.Mul(predA.orientation,predA.speed))

			fmt.Printf("PredA position @ t1 = %v E3(%v)\n", predA.position,predA.position.XYZ())						  

			/* predator orientated away from prey */
			predB := agent{position:space.E3Point( 5,0,5), orientation:space.E3Vector(1,0,1), speed:space.E3Scalar(5.0)} 
			So(predB,ShouldNotBeEmpty)


		})
	})
}
